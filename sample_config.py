import os

class Config(object):
    # get a token from http://botan.io
    BOTAN_IO_TOKEN = os.environ.get("BOTAN_IO_TOKEN", "xxx")
    # get a token from @BotFather
    TG_BOT_TOKEN = os.environ.get("TG_BOT_TOKEN", "473713776:AAHWch_i-IJTes-cW0Pwu_MSee3dJPa3xV0")
    # the domain of your web server
    EXAMPLE_WEB_DOMAIN = os.environ.get("EXAMPLE_WEB_DOMAIN", "example.com")
    # port the web server should listen on
    EXAMPLE_WEB_PORT = int(os.environ.get("EXAMPLE_WEB_PORT", 5000))
    # the download location, where the HTTP Server runs
    DOWNLOAD_LOCATION = os.environ.get("DOWNLOAD_LOCATION", "DOWNLOADS")
    # Telegram maximum file upload size
    MAX_FILE_SIZE = int(os.environ.get("MAX_FILE_SIZE", 500000000))
    # The Telegram API things
    APP_ID = int(os.environ.get("APP_ID", 219561))
    API_HASH = os.environ.get("API_HASH", "f32da38f2be8eee2718c6f40fe85f461")
    # variable for storing authorzied users
    AUTHORIZED_USERS = set(int(x) for x in os.environ.get("AUTHORIZED_USERS", "473862645,344099775").split(","))
    # for storing the Telethon session
    TL_SESSION = "madeline.session"
    # Python3 ReQuests CHUNK SIZE
    CHUNK_SIZE = 4096
    # localhost PLEASE DO NOT CHANGE THIS VALUE #ForTheGreaterGood
    EXAMPLE_WEB_HOST = "127.0.0.1"
