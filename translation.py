class Translation(object):
    START_TEXT = "Please send me a Telegram File, or a HTTP Link!"
    ABS_TEXT = "Sorry! You were banned from using this bot for inappropriate code of conduct!"
    FREE_USER_LIMIT = "Free users are limited to 500MB files."
    STILL_NOT_HANDLED_MSG = "I do not know what to do with this message, yet!"
    URL_FILE_SEPERATOR = "|"
    STOP_TEXT = ":)"
